import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import {
  SpecialAutocompleteComponent,
  SpecialCheckboxComponent,
  SpecialDatepickerComponent,
  SpecialDropdownComponent,
  SpecialFormModule,
  SpecialInputComponent,
  SpecialLabelComponent,
  SpecialMultipleAutocompleteComponent,
  SpecialRichtextComponent,
  SpecialTextAreaComponent,
  SpecialUploadComponent,
} from '../../components';
import { EControlTypes } from '../../core/aux-data/control-types.enum';
import { SpecialFormControl } from '../../core/forms/special-forms';
@Component({
  standalone: true,
  selector: 'sp-form-control-selector',
  templateUrl: './form-control-selector.component.html',
  styleUrls: ['./form-control-selector.component.scss'],
  encapsulation: ViewEncapsulation.None,
  imports: [
    CommonModule,
    SpecialMultipleAutocompleteComponent,
    SpecialDatepickerComponent,
    SpecialInputComponent,
    SpecialRichtextComponent,
    SpecialDropdownComponent,
    SpecialAutocompleteComponent,
    SpecialCheckboxComponent,
    SpecialTextAreaComponent,
    SpecialUploadComponent,
    SpecialLabelComponent,
    SpecialFormModule,
  ],
})
export class FormControlSelectorComponent implements OnInit {
  @Input() control!: SpecialFormControl<any>;

  get controlTypes(): typeof EControlTypes {
    return EControlTypes;
  }

  ngOnInit(): void {}
}
