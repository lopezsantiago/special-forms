import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroupViewerComponent } from './form-group-viewer.component';
import { SpecialFormBuilderService } from '../../core/services';
import { SpecialFormModule } from '../../components/special-form/special-form.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ControlDialogComponent } from './components/control-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormControlsListPipeModule } from '../../core/pipes/controls-list-pipe/controls-list.pipe.module';
import { FormControlSelectorComponent } from '../form-control-selector/form-control-selector.component';
@NgModule({
  declarations: [FormGroupViewerComponent, ControlDialogComponent],
  imports: [
    FormControlsListPipeModule,
    DragDropModule,
    CommonModule,
    SpecialFormModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    FormControlSelectorComponent,
  ],
  exports: [FormGroupViewerComponent],
  providers: [SpecialFormBuilderService],
})
export class FormGroupViewerModule {}
