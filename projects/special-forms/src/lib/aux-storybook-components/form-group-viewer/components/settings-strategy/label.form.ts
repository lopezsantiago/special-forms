import { ILabelSettings } from '../../../../components/special-label/special-label.interface';
import { EControlTypes } from '../../../../core/aux-data/control-types.enum';
import { IFormStructure } from '../../../../core/interfaces/form.interfaces';
import { CommonFormClass } from './common.form';
import { CommonFormCreator } from './common.form.interface';

export class LabelFormClass
  extends CommonFormClass
  implements CommonFormCreator
{
  public settingsFields(): IFormStructure {
    return {
      isLink: {
        label: 'Ideterminado',
        type: EControlTypes.checkbox,
      },
    };
  }

  public getSettings({ isLink }: { isLink: boolean }): ILabelSettings {
    return {
      isLink,
      onClickLink: () => {
        console.log('LINK');
      },
      pipe: () => 'value',
      stylesPipe: () => '',
    };
  }
}
