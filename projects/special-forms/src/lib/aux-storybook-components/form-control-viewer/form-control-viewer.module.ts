import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControlViewerComponent } from './form-control-viewer.component';
import { SpecialFormBuilderService } from '../../core/services';
import { FormControlSelectorComponent } from '../form-control-selector/form-control-selector.component';

@NgModule({
  declarations: [FormControlViewerComponent],
  imports: [
    CommonModule,
    FormControlSelectorComponent
  ],
  exports: [FormControlViewerComponent],
  providers: [SpecialFormBuilderService],
})
export class FormControlViewerModule {}
