import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy } from '@angular/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { InputMaskModule } from '@ngneat/input-mask';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { ErrorMessagePipeModule } from '../../core/pipes';
import { ITextAreaField } from './special-text-area.interface';

@Component({
  standalone:true,
  selector: 'sp-text-area',
  templateUrl: './special-text-area.component.html',
  styleUrls: ['./special-text-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports:[
    InputMaskModule,
    CommonModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule,
  ]
})
export class SpecialTextAreaComponent implements OnInit {
  @Input() control!: SpecialFormControl<ITextAreaField>;

  @Output() onBlur: EventEmitter<any> = new EventEmitter();
  @Output() onEnter: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
