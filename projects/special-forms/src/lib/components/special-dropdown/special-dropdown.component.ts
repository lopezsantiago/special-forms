import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { IDropdownSettings } from './special-dropdown.interface';
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {
  ErrorMessagePipeModule,
  TextByFunctionPipeModule,
} from '../../core/pipes';
@Component({
  standalone: true,
  selector: 'sp-dropdown',
  templateUrl: './special-dropdown.component.html',
  styleUrls: ['./special-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    MatSelectModule,
    ReactiveFormsModule,
    ErrorMessagePipeModule,
    MatIconModule,
    MatButtonModule,
    TextByFunctionPipeModule,
  ],
})
export class SpecialDropdownComponent {
  @Input() control!: SpecialFormControl<IDropdownSettings>;

  get settings(): IDropdownSettings {
    return this.control.settings;
  }

  iconClick(event: Event) {
    if (this.settings.iconAction) {
      this.settings.iconAction(this.control.value);
      event.stopPropagation();
    }
  }

  optionSelected(value: any) {
    if (this.settings.onSelect) this.settings.onSelect(value);
  }
}
