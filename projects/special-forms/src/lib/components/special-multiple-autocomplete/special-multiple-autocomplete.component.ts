import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import {
  MatAutocompleteModule,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { Subscription } from 'rxjs';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { IMultipleAutocompleteSettings } from './special-multiple-autocomplete.interface';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { MatChipsModule } from '@angular/material/chips';
import { ErrorMessagePipeModule, TextByFunctionPipeModule } from '../../core/pipes';

@Component({
  standalone: true,
  selector: 'sp-multiple-autocomplete',
  templateUrl: './special-multiple-autocomplete.component.html',
  styleUrls: ['./special-multiple-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatInputModule,
    CommonModule,
    MatAutocompleteModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule,
    TextByFunctionPipeModule,
    MatChipsModule,
  ],
})
export class SpecialMultipleAutocompleteComponent {
  @Input() control!: SpecialFormControl<IMultipleAutocompleteSettings>;

  subs = new Subscription();
  internalControl = new FormControl();

  constructor() {}

  ngOnInit() {
    this.init();
  }

  get settings(): IMultipleAutocompleteSettings {
    return this.control.settings;
  }

  init() {
    this.subs.add(
      this.internalControl.valueChanges
        .pipe(debounceTime(500))
        .subscribe((value) => {
          if (typeof value === 'string') {
            this.control.settings.getData(value, this.control);
          }
        })
    );
  }

  optionSelected(data: MatAutocompleteSelectedEvent) {
    const newItem = data.option.value;
    this.internalControl.reset();
    this.control.setValue([
      ...(this.control.value || []).filter(
        (item: any) =>
          item[this.control.settings.fieldId] !==
          newItem[this.control.settings.fieldId]
      ),
      newItem,
    ]);

    if (this.settings.onSelect) this.settings.onSelect(newItem);
  }

  iconClick(event: Event) {
    if (this.settings.iconAction) {
      this.settings.iconAction(this.control.value);
      event.stopPropagation();
    }
  }

  remove(value: any[]): void {
    let currentValues = Array.from(this.control.value);
    const index = currentValues.indexOf(value);

    if (index > -1) {
      currentValues.splice(index, 1);
      const value = currentValues;
      this.control.markAsDirty();
      this.control.setValue(value);
    }
  }

  configValue = (item: any) => {
    if (!item) return '';
    const fieldName = this.control.settings.fieldName;
    return fieldName instanceof Function ? fieldName(item) : item[fieldName];
  };

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
