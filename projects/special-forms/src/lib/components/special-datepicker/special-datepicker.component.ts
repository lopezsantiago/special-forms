import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { ErrorMessagePipeModule } from '../../core/pipes';
import { IDatePickerSettings } from './special-datepicker.interface';
@Component({
  standalone: true,
  selector: 'sp-datepicker',
  templateUrl: './special-datepicker.component.html',
  styleUrls: ['./special-datepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatInputModule,
    MatDatepickerModule,
    CommonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule,
    MatNativeDateModule,
  ],
})
export class SpecialDatepickerComponent implements OnInit {
  @Input() control!: SpecialFormControl<IDatePickerSettings>;

  constructor() {}

  get startAt() {
    return this.control.settings?.startAt || new Date();
  }

  ngOnInit(): void {}
}
