import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { IRichTextSettings } from './special-richtext.interface';
import { NgxEditorModule, Toolbar } from 'ngx-editor';
import { Editor } from 'ngx-editor';

@Component({
  standalone: true,
  selector: 'sp-input',
  templateUrl: './special-richtext.component.html',
  styleUrls: ['./special-richtext.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatIconModule, ReactiveFormsModule, NgxEditorModule],
})
export class SpecialRichtextComponent implements OnInit {
  editor: Editor;
  formControl: SpecialFormControl<IRichTextSettings>;
  colorPresets = ['red', '#FF0000', 'rgb(255, 0, 0)'];

  @Input() set control(control: SpecialFormControl<IRichTextSettings>) {
    this.formControl = control;
    this.editor = new Editor();
  }

  toolbar: Toolbar = [
    // default value
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
    ['horizontal_rule', 'format_clear'],
  ];

  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.editor.destroy();
  }
}
