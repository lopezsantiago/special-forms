import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SpecialRichtextComponent } from './special-richtext.component';

describe('SpecialRichtextComponent', () => {
  let component: SpecialRichtextComponent;
  let fixture: ComponentFixture<SpecialRichtextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialRichtextComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpecialRichtextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
