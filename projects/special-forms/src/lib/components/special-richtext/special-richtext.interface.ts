import { InputmaskOptions } from '@ngneat/input-mask';
import { EControlTypes } from '../../core/aux-data/control-types.enum';
import { IFieldData } from '../../core/interfaces/field-basics.interfaces';

export type IRichTextSettings = {

};

export interface IRichTextField extends IFieldData {
  settings: IRichTextSettings;
  type: EControlTypes.input;
}
