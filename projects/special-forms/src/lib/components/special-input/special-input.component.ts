import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { InputMaskModule } from '@ngneat/input-mask';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { ErrorMessagePipeModule } from '../../core/pipes';
import { IInputSettings } from './special-input.interface';
@Component({
  standalone:true,
  selector: 'sp-input',
  templateUrl: './special-input.component.html',
  styleUrls: ['./special-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports:[
    InputMaskModule,
    CommonModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule
  ]
})
export class SpecialInputComponent implements OnInit {
  @Input() control!: SpecialFormControl<IInputSettings>;

  constructor() {}

  ngOnInit(): void {}

  onEnterClick() {
    if (this.settings.onEnter) {
      this.settings.onEnter(this.control.value);
    }
  }

  onBlurAction() {
    if (this.settings.onBlur) {
      this.settings.onBlur(this.control.value);
    }
  }

  get settings(): IInputSettings {
    return this.control.settings;
  }

  iconClick(event:Event) {
    if (this.settings.iconAction) {
      this.settings.iconAction(this.control.value);
      event.stopPropagation();
    }
  }
}
