import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ErrorMessagePipeModule } from '../../core/pipes';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { ICheckboxSettings } from './special-checkbox.interface';

@Component({
  standalone: true,
  selector: 'sp-checkbox',
  templateUrl: './special-checkbox.component.html',
  styleUrls: ['./special-checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    MatCheckboxModule,
    CommonModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule,
  ],
})
export class SpecialCheckboxComponent implements OnInit {
  @Input() control!: SpecialFormControl<ICheckboxSettings>;
  constructor() {}
  ngOnInit(): void {}
}
