import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { IUploadSettings } from './special-upload.interface';
import { DomSanitizer } from '@angular/platform-browser';
import { ChangeDetectionStrategy } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ErrorMessagePipeModule } from '../../core/pipes';

@Component({
  standalone: true,
  selector: 'sp-upload',
  templateUrl: './special-upload.component.html',
  styleUrls: ['./special-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    NgxDropzoneModule,
    CommonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    ErrorMessagePipeModule,
  ],
})
export class SpecialUploadComponent implements OnInit {
  control!: SpecialFormControl<IUploadSettings>;

  @Input('control') set controlSetter(
    control: SpecialFormControl<IUploadSettings>
  ) {
    this.control = control;
    this.previewImages = control.value;
  }

  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  previewImages: any[] = [];

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  onSelectMultiple(event: any) {
    this.previewImages = [
      ...this.previewImages,
      ...event.addedFiles.map((file: File) => ({
        lastModified: file.lastModified,
        url: this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file)),
      })),
    ];

    this.control.setValue([
      ...this.control.value,
      ...event.addedFiles.map(this.fixFileName),
    ]);
    this.onSelect.emit(this.control.value);
    this.control.markAsDirty();
  }

  onSelectOne(event: any) {
    this.previewImages = event.addedFiles.map((file: File) => ({
      lastModified: file.lastModified,
      url: this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file)),
    }));

    this.control.setValue(event.addedFiles.map(this.fixFileName));
    this.onSelect.emit(this.control.value);
    this.control.markAsDirty();
  }

  fixFileName(file: File) {
    return Object.defineProperty(file, 'name', {
      writable: true,
      value: file.name.normalize('NFD').replace(/[\u0300-\u036f]/g, ''),
    });
  }

  clean() {
    this.previewImages = [];
  }
  onRemove(file: File) {
    this.control.setValue(
      this.control.value.filter(
        (fl: any) => fl.lastModified !== file.lastModified
      )
    );
    this.previewImages = this.previewImages.filter(
      (fl) => fl.lastModified !== file.lastModified
    );
  }
}
