import { EControlTypes } from '../../core/aux-data/control-types.enum';
import { IFieldData } from '../../core/interfaces/field-basics.interfaces';

export type ILabelSettings = {
  pipe?: (value: any) => string;
  stylesPipe?: (value: any) => string;
  isLink: boolean;
  onClickLink?: (value: any) => void;
};

export interface ILabelField extends IFieldData {
  settings: ILabelSettings;
  type: EControlTypes.label;
}
