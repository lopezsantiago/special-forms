import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { SpecialFormControl } from '../../core/forms/special-forms';
import { TextByFunctionPipeModule } from '../../core/pipes';
import { ILabelSettings } from './special-label.interface';

@Component({
  standalone: true,
  selector: 'sp-label',
  templateUrl: './special-label.component.html',
  styleUrls: ['./special-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatIconModule, TextByFunctionPipeModule],
})
export class SpecialLabelComponent implements OnInit {
  @Input() control!: SpecialFormControl<ILabelSettings>;

  constructor() {}

  ngOnInit(): void {}

  get settings(): ILabelSettings {
    return this.control.settings || { isLink: false };
  }

  onLink() {
    if (this.settings.isLink && this.settings.onClickLink) {
      this.settings.onClickLink(this.control.value);
    }
  }
}
