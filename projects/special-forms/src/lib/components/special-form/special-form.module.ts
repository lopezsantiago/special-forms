import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormControlsRenderDirective,
  SpecialFormComponent,
  SpecialArrayComponent,
} from './special-form.component';
import { MatButtonModule } from '@angular/material/button';
import { FormControlsListPipeModule } from '../../core/pipes/controls-list-pipe/controls-list.pipe.module';
import { SpecialDropdownComponent } from '../special-dropdown/special-dropdown.component';
import { SpecialInputComponent } from '../special-input/special-input.component';
import { SpecialAutocompleteComponent } from '../special-autocomplete/special-autocomplete.component';
import { SpecialDatepickerComponent } from '../special-datepicker/special-datepicker.component';
import { SpecialLabelComponent } from '../special-label/special-label.component';
import { SpecialMultipleAutocompleteComponent } from '../special-multiple-autocomplete/special-multiple-autocomplete.component';
import { SpecialCheckboxComponent } from '../special-checkbox/special-checkbox.component';
import { SpecialTextAreaComponent } from '../special-text-area/special-text-area.component';
import { SpecialUploadComponent } from '../special-upload/special-upload.component';
import { SpecialRichtextComponent } from '../special-richtext/special-richtext.component';

@NgModule({
  declarations: [
    SpecialArrayComponent,
    FormControlsRenderDirective,
    SpecialFormComponent,
  ],
  imports: [
    CommonModule,
    FormControlsListPipeModule,
    MatButtonModule,
    SpecialRichtextComponent,
    SpecialDropdownComponent,
    SpecialInputComponent,
    SpecialAutocompleteComponent,
    SpecialDatepickerComponent,
    SpecialLabelComponent,
    SpecialMultipleAutocompleteComponent,
    SpecialCheckboxComponent,
    SpecialTextAreaComponent,
    SpecialUploadComponent,
  ],
  exports: [
    SpecialArrayComponent,
    FormControlsRenderDirective,
    SpecialFormComponent,
  ],
})
export class SpecialFormModule {}
